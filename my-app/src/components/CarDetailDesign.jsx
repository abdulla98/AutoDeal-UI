import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import '../details.css'
class CarDetailDesign extends Component {
    getHp = (power) => {
        return power * 1.61;
    }
    goBack=()=> {
        window.history.back();
      }
    render() {
        return (
            <div>
          
      <div className='row details-page container'>
        <div className='col container details-design-firstpart'> 
                    <div className="details-img-place">
                        <img className="img-fluid img-size" src={this.props.image}/>
                            {/* <ul></ul>
                            <h5>Author</h5>
                            <li>Email: {this.props.email}</li>
                            <li>username: {this.props.username}</li>
                            <li>Name:  {this.props.name}</li>
                        <li>Address: {this.props.address}</li> */}
                    </div>             
        </div>
        <div className="col">
            <h1 className="title">{this.props.make} {this.props.model}</h1>
            <h3 className="my-3 project-title">Car Details:</h3>
            <br />
            <h5> {this.props.description} <br/><br/></h5>
            <ul>
                <li><b>Registration:&nbsp;</b> <span>{this.props.registration}</span> </li>
                <li><b>Mileage:&nbsp;</b><span>{this.props.mileage}</span></li>
                <li><b>price:&nbsp;</b><span>{this.props.price} $</span></li>
                <li><b>Power:&nbsp;</b><span>{this.props.power} KW</span></li>
                {/* </span></li><li><b>Horse Power:&nbsp;</b><span>{this.getHp} HP</span></li> */}
                <li><b>Fuel:&nbsp;</b><span>{this.props.fuel}</span></li>

            </ul>
            <button onClick={this.goBack} className='btn back-button btn-primary'>Back</button>
        </div>        

      </div>         
      </div>
      )
    }
}

export default CarDetailDesign;