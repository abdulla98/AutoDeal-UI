import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import CardDetailDesign from './CarDetailDesign'
import Navigation from './Navigation'
class CarDetail extends Component {
    state ={
        data: [],
        url:''
    }
    componentDidMount() {
        //kjo o url-ja jem, ju bonja urln tuj g vu bohet run rest api.
        const id = this.props.match.params.id;
        
        const url = `http://localhost:8080/cars?id=${id}`
        this.setState({
            url: url
        })
        console.log(this.url)
        fetch(url)
            .then(res => res.json())
            .then(json => this.setState({ data: json.data })); }
    render() {
        console.log(this.state.data)
        return (
            <div>
                <Navigation />
                {this.state.data.map((car =>
                    <CardDetailDesign
                        make={car.make}
                        model={car.model}
                        registration={car.date}
                        mileage={car.mileage}
                        fuel={car.fuel}
                        power={car.power}
                        image={car.image}
                        description={car.description}
                        price={car.price}
                    />
                ))}
                </div>
            )
        }

}

export default CarDetail;