import React, { Component } from 'react';
import logo from '../../images/cardealLogo.png';
import '../../auth.css'
import { Link } from 'react-router-dom'




class Login extends Component {
    state = {
        username: "",
        password: "",
        errors: [],
        loading: false
    }
    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
        
    };
    done = () => {
        console.log('done')
    }
   
  isFormValid = ({ username, password }) => {
    let errors = [];
    let error;
    if (this.isFormEmpty(this.state)) {
      error = { message: 'Fill in all fields!' }
      this.setState({ errors: errors.concat(error) });
      return false;
  }
    return username && password;
  }

    isFormEmpty = ({ username, password}) => {
      return !username.length || !password.length;
  }
            
  displayError = errors => {
    return errors.map((errors, i) => <i key={i}>{errors.message}</i>)
    }
  handleSubmit = event => {
    event.preventDefault();
    if (this.isFormValid(this.state)) {
      this.setState({ errors: [], loading: true });
      
    }
   } 
    render() {
        const { username, password } = this.state;
        return (
        
        <div className='container card-auth card-border'>
            <div className="card-body">
                <h1 className='App card-title'><img src={logo} alt='hub shortcut' /></h1>
            </div>
                <form action="http://localhost/car-dealing-rest-API/api/user/login.php" method="post" onSubmit={this.handleSubmit}>
                    <div className='form-group' >
                        <input name="username" onChange={this.handleChange} type="text" className="form-control" id="exampleInputUsername1" aria-describedby="usernameHelp" placeholder='Enter username' />
                        <input name="password" onChange={this.handleChange} type="password" className="form-control" id="exampleInputPassword1" placeholder="Enter password"></input>
                        <button disabled={this.state.loading} className={this.state.loading ? 'loading' : ''} type="submit" className="btn btn-primary-auth btn-block">Login</button>
                    </div>
                    {this.state.errors.length > 0 && (
                        <div className="alert alert-danger alert-manual" role="alert">
                            {this.displayError(this.state.errors)}
                        </div>)}
               <hr className="hr"/> 
            </form>
            <div className='App'><p className="inline-block text-muted">Not a user? </p><Link to="/register" className="card-link">Register</Link></div>

        </div>
    )
  }
}

const mapStateFromProps = state => ({
  isLoading: state.user.isLoading
  
});
export default Login;
 