# AutoDeal-UI

This is the AutoDeal User Interface website, it has been created by using React.js

## Getting Started

### Installing

A step by step series of examples that tell you how to get a development env running

In order to run the website on you computer you have to install:  ``` https://nodejs.org  ``` <br />
Install yarn: ``` choco install yarn ``` <br />
One editor ex:  ``` https://www.jetbrains.com/webstorm/  ``` <br />

You can clone the website on you machine by typing on you terminal git clone and the link here: ``` https://github.com/agniramadani/AutoDeal-UI.git ```


## Built With

* [React.js](https://reactjs.org/) - React is a JavaScript library for building user interfaces.
* [Bootstrap](https://getbootstrap.com/) - Bootstrap is an open source toolkit for developing with HTML, CSS, and JS
* [Font Awesome](https://fontawesome.com/) - Font Awesome is a font and icon toolkit based on CSS and LESS.


## Contributors
  
 * [Agni Ramadani](https://github.com/agniramadani)
 * [Abdulla Bakija](https://github.com/abdulla98)
 * [Ezan Iljazi](https://github.com/ezan99)
 
## User Interface Description
- The websiete is responsive in all platforms. (desktop, mobile, tablet) <br />
- When opening the application, the homepage have just the search form, from which the user can give his criteria.<br />
- The design and style is friendly to online readers. <br />
- The app maintain interactive elements, noticeable buttons, legible fonts etc.. <br />
- App uses flash animation, moving text, fancy cursors etc.. <br />
- It has a good navigation and very easy for the costumers to use. <br />
